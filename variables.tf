variable "env" {
  type    = string
  default = "dev"
}
variable "gitlab_runner_register_token" {
  type    = string
  sensitive = true
}